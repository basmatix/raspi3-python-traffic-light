import sys
import RPi.GPIO as GPIO

import syslog

GPIO.setmode(GPIO.BOARD)

RED=11
AMBER=13
GREEN=15

GPIO.setup(RED, GPIO.OUT)
GPIO.setup(AMBER, GPIO.OUT)
GPIO.setup(GREEN, GPIO.OUT)

if len(sys.argv) >= 4:
    syslog.syslog(syslog.LOG_DEBUG, 'setting lights ' + str(sys.argv[1:len(sys.argv)]))
    GPIO.output(RED,int(sys.argv[1]))
    GPIO.output(AMBER,int(sys.argv[2]))
    GPIO.output(GREEN,int(sys.argv[3]))
else:
    syslog.syslog(syslog.LOG_ERR, 'wrong usage, call python traffic-light.py <red> <yellow> <green>')
