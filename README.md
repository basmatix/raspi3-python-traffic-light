# Traffic light controller for RaspberryPi 3

A very basic controller interface to set LEDs on or off in a traffic light way.

## Pinout

    O
    
    0 0  - = 0V
    0 0  r = red LED
    0 -  y = yellow LED
    0 0  g = green LED
    0 0
    r 0
    y 0
    g 0
    0 0
    . .
    . .
    . .
    
    O
    
    ethernet usb usb
    
Or as picture
#+html: <p align="center"><img src="pinout.jpg" /></p>

## Usage

`python traffic-light.py <red> <yellow> <green>` where 0 means off and >0 means on for red, yellow and green